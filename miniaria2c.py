#!/usr/bin/env python3

#############################################################
# Mini Aria2 client                                         #
# Dec 21, 2020                                              #
# By Gydo194                                                #
#                                                           #
# Licensed under BSD 2-clause license - see LICENSE file    #
#############################################################

import json
import sys
import requests
from requests.exceptions import HTTPError
from argparse import ArgumentParser

aria2_default_options = {
        "continue"                  : "true",
        "max-connection-per-server" : 15,
        "split"                     : 15,
        "min-split-size"            : "10M",
        "user-agent"                : "Mozilla/5.0 (X11; Linux; rv:5.0) Gecko/5.0 Firefox/5.0"
}

aria2_rpc_default = {
        "jsonrpc"   : "2.0",
        "id"        : "gydo194/miniaria2c",
        "params"    : []
        # Method attribute added in by operation functions
}

parser = ArgumentParser()
parser.add_argument("-o", "--operation", help = "Operation to perform", type = str, required = False, default = "download")
parser.add_argument("-r", "--rpc-url", help = "RPC url", type = str, required = True)
parser.add_argument("-u", "--file-uri", help = "URI to download", type = str, required = True)

options = parser.parse_args()

def aria2_op_add_uri():
    #clone options dicts
    
    aria2_options = aria2_default_options.copy()
    request = aria2_rpc_default.copy()

    request["method"] = "aria2.addUri"
    request["params"].append([options.file_uri])
    request["params"].append(aria2_options)

    json_request = json.dumps(request)

    response = requests.post(options.rpc_url, data = json_request)

    response.raise_for_status()

#operation selector jump table
operations = {
        "download"      : aria2_op_add_uri
}

def execute_operation():
    requested_operation = options.operation

    if not requested_operation in operations:
        print(f"Unknown operation {requested_operation}")
        return False

    funcp = operations[requested_operation]

    try:
        funcp()
    except HTTPError as e:
        print(f"HTTP Error from server: {e.response.status_code} {e.response.reason}")
        sys.exit(1)
    except Exception as e:
        print(f"An error occurred: {e}")
        sys.exit(2)


if "__main__" == __name__:
    execute_operation()
